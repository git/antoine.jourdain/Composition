<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use GuzzleHttp\Client;

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->addPsr4('BL\\', __DIR__);
$app = AppFactory::create();

$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);

$proxy1='192.168.128.139:8080';//http://193.49.118.36:8080';
$proxy2='proxycl.iut.uca.fr ';//http://193.49.118.36:8080';
$ip_AccManager="192.168.127.122:8080";
$ip_AppManager="192.168.127.122:8082";

function humain($ip_AppManager, $approval_id, Response $response, $account_id)
{
    $manager = new Client(['base_uri' => $ip_AppManager]);
    $res_approved = $manager->request('GET', '/approval/approvals/' . $approval_id);
    if (str_contains($res_approved->getBody(), "APPROVED")) $response->getBody()->write("Approved loan for account n°" . $account_id . ". Approval id : " . $approval_id);
    else $response->getBody()->write("Refused loan for account n°" . $account_id . ". Approval id : " . $approval_id);
}

$app->post('/loan', function(Request $request, Response $response, $args) use ($ip_AccManager, $ip_AppManager, $proxy1, $proxy2) {
    try{
        $data = $request->getParsedBody();
        $somme = $data['somme'];
        $account_id = $data['account_id'];
        $approval_id = $data['approval_id'];

        if($somme >= 10000){
            try{
                humain($ip_AppManager, $approval_id, $response, $account_id);
                return $response->withHeader('Content-type', 'text/html')->withStatus(200);
            }
            catch (ConnectException $ee){ throw new Exception("Error in URL"); }
            catch (ClientException $e) {
                throw new Psr7\Message::toString($e->getRequest());
                throw new Psr7\Message::toString($e->getResponse());
            }
        }
        else{
            try{
                $client = new Client(['base_uri' => $ip_AccManager]);
                $res_acc = $client->request('GET', '/account/Accounts/' . $account_id, ['proxy'=>['http' => $proxy1, 'https'=> $proxy2]]);
                if ($res_acc->getBody() == 'high'){
                    humain($ip_AppManager, $approval_id, $response, $account_id);
                }
                else{
                    $response->getBody()->write("Approved loan for account n°" . $account_id . ". Approval id : " . $approval_id);
                }
                return $response->withHeader('Content-type', 'text/html')->withStatus(200);
            }
            catch (ConnectException $ee){ throw new Exception("Error in URL"); }
            catch (ClientException $e) {
                throw new Psr7\Message::toString($e->getRequest());
                throw new Psr7\Message::toString($e->getResponse());
            }
        }
    }
    catch(Exception $e){
        throw new HttpInternalServerErrorException();
    }
});

$app->run();
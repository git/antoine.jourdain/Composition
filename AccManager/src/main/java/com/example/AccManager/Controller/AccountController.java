package com.example.AccManager.Controller;


import com.example.AccManager.Erreur.AccountForbiddenException;
import com.example.AccManager.Erreur.AccountNotFoundException;
import com.example.AccManager.AccountRepository;
import com.example.AccManager.model.Account;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountRepository repository;


    public AccountController(AccountRepository repository) {
        this.repository = repository;
        repository.save(new Account(1500,"high"));
        repository.save(new Account(10000,"low"));
        repository.save(new Account(10,"high"));
    }

    @RequestMapping(value = "/Accounts", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Iterable<Account> accounts() {
        try {
            return repository.findAll();
        } catch (Exception e){
            throw new AccountNotFoundException(e.getMessage());
        }

    }

    @GetMapping(value = "/Accounts/{account_id}", produces = "application/json")
    public @ResponseBody String account(@PathVariable("account_id") long account_id) {
        try {
            Account account = repository.findById(account_id);
            return account.getRisk();
        } catch (Exception e){
            throw new AccountNotFoundException(e.getMessage());
        }

    }
    @RequestMapping(value="/Accounts",method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody Account add(@RequestBody Account a)
    {
        try{
            repository.save(a);
            return a;
        }
        catch (Exception e)
        { throw new AccountForbiddenException(e.getMessage()); }
    }



}

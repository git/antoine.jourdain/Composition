package com.example.AccManager;

import com.example.AccManager.model.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account,Long> {
   Account findById(long id);
}

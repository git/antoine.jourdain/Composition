package com.example.AccManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
//@ComponentScan({"com.example.AccManager.Controller","come.example.AccManager.model"})
@EntityScan("com.example.AccManager")
@SpringBootApplication
public class AccManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccManagerApplication.class, args);
	}

}

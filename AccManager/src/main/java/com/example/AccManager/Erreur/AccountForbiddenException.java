package com.example.AccManager.Erreur;

public class AccountForbiddenException extends RuntimeException{
    public AccountForbiddenException(String exception){
        super(exception);
    }

}

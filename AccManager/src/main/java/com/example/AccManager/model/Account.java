package com.example.AccManager.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long account_id;

    @Column(name="somme")
    private int somme;

    @Column(name="risk")
    private String risk;

    public Account(){ }

    public Account( int somme, String risk) {
        this.somme = somme;
        this.risk = risk;
    }
}

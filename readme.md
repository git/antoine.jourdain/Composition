# Composition de Services Web

Ce projet a pour but de développer une compisition de services web en utilisant différentes technologies tels que PHP Slim ou encore Springboot.

Cette composition a été réalisé dans le cadre de notre deuxième année de BUT Informatique et le sujet de notre TP était de reproduire l'infrastructure ci-dessous.

![Schéma du service à réaliser ](https://cdn.discordapp.com/attachments/612801733109874688/1226559202764259398/loan.png?ex=66253546&is=6612c046&hm=9a3752a19deb601ca1504f4387cd905ff40dc8f9c1cbeafca0fa8d63be944a86&)
## Authors

- [Maxence JOUANNET](https://codefirst.iut.uca.fr/git/maxence.jouannet)
- [Antoine JOURDAIN](https://codefirst.iut.uca.fr/git/antoine.jourdain)
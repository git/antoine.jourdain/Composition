package com.example.AppManager.Erreur;

public class ApprovalForbiddenException extends RuntimeException {

    public ApprovalForbiddenException(String exception){
        super(exception);
    }

}

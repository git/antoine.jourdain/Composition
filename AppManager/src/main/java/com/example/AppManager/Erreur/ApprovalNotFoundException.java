package com.example.AppManager.Erreur;

public class ApprovalNotFoundException extends RuntimeException{
    public ApprovalNotFoundException(String message) {
        super(message);
    }
}

package com.example.AppManager;


import com.example.AppManager.Model.Approval;
import org.springframework.data.repository.CrudRepository;

public interface ApprovalRepository extends CrudRepository<Approval,Long> {
    Approval findById(long id);
}

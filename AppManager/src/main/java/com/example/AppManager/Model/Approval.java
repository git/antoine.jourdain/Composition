package com.example.AppManager.Model;



import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
public class Approval {

    public enum ResponseType {APPROVED, REFUSED}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long approval_id;

    @Column(name="response")
    private ResponseType responseType;

    public Approval(){ }

    public Approval (ResponseType responseType) {
        this.responseType = responseType;
    }
}

package com.example.AppManager.Controller;

import com.example.AppManager.ApprovalRepository;
import com.example.AppManager.Erreur.ApprovalForbiddenException;
import com.example.AppManager.Erreur.ApprovalNotFoundException;
import com.example.AppManager.Model.Approval;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/approval")
public class ApprovalController {

    private final ApprovalRepository repository;


    public ApprovalController(ApprovalRepository repository) {
        this.repository = repository;
        repository.save(new Approval(Approval.ResponseType.APPROVED));
        repository.save(new Approval(Approval.ResponseType.REFUSED));
        repository.save(new Approval(Approval.ResponseType.APPROVED));
    }

    @RequestMapping(value = "/approvals", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Iterable<Approval> accounts() {
        try {
            return repository.findAll();
        } catch (Exception e){
            throw new ApprovalNotFoundException(e.getMessage());
        }

    }

    @GetMapping(value = "/approvals/{approval_id}", produces = "application/json")
    public @ResponseBody Approval.ResponseType account(@PathVariable("approval_id") long approval_id) {
        try {
            Approval account = repository.findById(approval_id);
            return account.getResponseType();
        } catch (Exception e) {
            throw new ApprovalNotFoundException(e.getMessage());
        }

    }

    @PostMapping(value = "/approvals", consumes = "application/json", produces = "application/json")
    public @ResponseBody Approval addApproval(@RequestBody Approval approval) {
        try {
            return repository.save(approval);
        } catch (Exception e){
            throw new ApprovalForbiddenException(e.getMessage());
        }

    }

    @DeleteMapping(value = "/approvals/{approval_id}")
    public ResponseEntity<Approval> deleteApproval(@PathVariable("approval_id") long approval_id) {
        if (repository.existsById(approval_id)) {
            repository.deleteById(approval_id);
            return ResponseEntity.ok().build();
        } else {
           throw new ApprovalNotFoundException("ERROR !");
        }
    }




}

